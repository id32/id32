============
ID32 project
============

[![build status](https://gitlab.esrf.fr/ID32/id32/badges/master/build.svg)](http://ID32.gitlab-pages.esrf.fr/ID32)
[![coverage report](https://gitlab.esrf.fr/ID32/id32/badges/master/coverage.svg)](http://ID32.gitlab-pages.esrf.fr/id32/htmlcov)

ID32 software & configuration

Latest documentation from master can be found [here](http://ID32.gitlab-pages.esrf.fr/id32)
