# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import sleep

from bliss import setup_globals
from bliss.setup_globals import *
from bliss import current_session
from bliss.config.settings import HashSetting
from bliss.common.logtools import elog_info, elog_error
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.common.shutter import BaseShutterState
from bliss.shell.getval import getval_yes_no


class ID32EScanPreset(ScanPreset):
    def __init__(self, waitforbeamrefill_obj):
        self._wf = waitforbeamrefill_obj

    def prepare(self, scan):
        title = scan.scan_info["title"]
        self._session = scan.scan_info["session_name"]
        t = title.split()
        t[0] = "escan"
        title = " ".join(x for x in t)
        num = scan.scan_number
        elog_info(f"#S{int(num)} {title} launched on {date()}")

        scan_duration = scan.scan_info["npoints"] * float(t[5]) + 100

        self._wf.do_waitforbeam()
        self._wf.do_waitforrefill(scan_duration)

        if self._session == "rixs":
            multiplexer_exph.switch("hutch_select_mono", "rixs")
            multiplexer_rixs.switch("acq_trig_rixs", "zap")

        elif self._session == "xmcd":
            multiplexer_exph.switch("hutch_select_mono", "xmcd_diff")
            multiplexer_xmcd.switch("hutch_select_xmcd", "xmcd")
            multiplexer_xmcd.switch("acq_trig_xmcd", "fscan_mono")

        elif self._session == "fourc_diff":
            multiplexer_exph.switch("hutch_select_mono", "xmcd_diff")
            multiplexer_xmcd.switch("hutch_select_xmcd", "diff")
            multiplexer_xmcd.switch("acq_trig_xmcd", "fscan_mono")
        else:
            pass


class ID32ChainPreset(ChainPreset):
    def __init__(self, waitforbeamrefill_obj):
        self._wf = waitforbeamrefill_obj

    def prepare(self, acq_chain):
        title = acq_chain.scan.scan_info["title"]
        num = acq_chain.scan.scan_number
        self._session = acq_chain.scan.scan_info["session_name"]
        elog_info(f"#S{int(num)} {title} launched on {date()}")

    def start(self, acq_chain):
        if self._session == "rixs":
            multiplexer_rixs.switch("acq_trig_rixs", "count")

        elif self._session == "xmcd":
            multiplexer_xmcd.switch("hutch_select_xmcd", "xmcd")
            multiplexer_xmcd.switch("acq_trig_xmcd", "count")
            self._xmcd_shutter_was = multiplexer_xmcd.getOutputStat("shutter_xmcd")
            # multiplexer_xmcd.switch("shutter_xmcd", "open")

        elif self._session == "fourc_diff":
            multiplexer_xmcd.switch("hutch_select_xmcd", "diff")
            multiplexer_xmcd.switch("acq_trig_xmcd", "count")

        else:
            pass

    def stop(self, acq_chain):
        if self._session == "xmcd" and self._xmcd_shutter_was == "CLOSE":
            # multiplexer_xmcd.switch("shutter_xmcd", "close")
            pass

    class Iterator(ChainIterationPreset):
        def __init__(self, iteration_nb, preset):
            self.preset = preset
            self.iteration = iteration_nb

        def prepare(self):
            self.preset._wf.do_waitforbeam()

        def start(self):
            pass

        def stop(self):
            pass

    def get_iterator(self, acq_chain):
        iteration_nb = 0
        while True:
            yield ID32ChainPreset.Iterator(iteration_nb, self)
            iteration_nb += 1


class WaitForBeamAndOrRefill:
    def __init__(self):

        self._session = current_session.name
        # Create a Hash beacon setting PER session !!!
        wf_params = {
            "wait_for_refill": False,
            "wait_for_beam": False,
            "wfr_stable_time": 100,
            "wfb_stab_time": 100,
            "wfr_check_time": 0,
            "simul": False,
            "simul_beamlost": False,
            "simul_injection": False,
        }
        self._params = HashSetting(
            f"wait_for_beamrefill_{self._session}", default_values=wf_params
        )

    def simul(self, simul: bool = True, session_name: str = None):
        if session_name is None:
            session_name = self._session
        yes = getval_yes_no(
            f"Are you sure to set simulation to {simul} on {session_name} session ?",
            False,
        )
        if yes:
            param = HashSetting(f"wait_for_beamrefill_{session_name}")
            param["simul"] = simul

    def simul_beamlost(self, lost: bool = True, session_name: str = None):
        if session_name is None:
            session_name = self._session
        yes = getval_yes_no(
            f"Are you sure to simul beamlost to {lost} on {session_name} session ?",
            False,
        )
        if yes:
            param = HashSetting(f"wait_for_beamrefill_{session_name}")
            param["simul_beamlost"] = lost

    def simul_injection(self, injection: bool = True, session_name: str = None):
        if session_name is None:
            session_name = self._session
        yes = getval_yes_no(
            f"Are you sure to simulate injection to {injection} on {session_name} session ?",
            False,
        )
        if yes:
            param = HashSetting(f"wait_for_beamrefill_{session_name}")
            param["simul_injection"] = injection

    def waitforbeam(self, wait=True, stab_waittime=100):
        _on = "ON" if wait else "OFF"
        print(f'"Wait For Beam" is now {_on}')
        self._params["wait_for_beam"] = wait
        self._params["wfb_stab_time"] = stab_waittime

    def waitforrefill(self, wait=True, check_time=0, stab_waittime=100):
        _on = "ON" if wait else "OFF"
        print(f'"Wait For Refill" is now {_on}')
        self._params["wait_for_refill"] = wait
        self._params["wfr_check_time"] = check_time
        self._params["wfr_stab_time"] = stab_waittime

    def get_safe_machinfo_attr(self, attr_to_read):
        # Repeat the reading 4 times.
        # timeout is already set to 200ms to reduce wait time.
        attr = None
        for i in range(4):
            try:
                (attr,) = machinfo.proxy.read_attributes((attr_to_read,))
                break
            except Exception:
                print(
                    f"Timeout ({machinfo.proxy.get_timeout_millis()} ms) reading machine server {machinfo.tango_uri} -> retrying"
                )
                sleep(0.5)
        # last chance to read
        if attr is None:
            (attr,) = machinfo.proxy.read_attributes((attr_to_read,))
        return attr.value

    def get_sr_mode(self):
        mode = self.get_safe_machinfo_attr("SR_Mode")
        return machinfo.SRMODE(mode).name

    def get_injection_mode(self):
        mode = self.get_safe_machinfo_attr("Mode")
        return machinfo.INJECTION_MODE(mode).name

    def get_sr_current(self):
        return self.get_safe_machinfo_attr("SR_Current")

    def get_sr_refill_countdown(self):
        return self.get_safe_machinfo_attr("SR_Refill_Countdown")

    def do_waitforbeam(self):

        if self._params["wait_for_beam"]:
            do_stabilization_wait = False

            # Here we did optimize nb of calls to machine and fe devices
            # remember this function is called at each scan iteration
            # to much calls will cost a lot about total scan spent time.

            # in preamble inform the elogbook
            ring_current = self.get_sr_current()
            if ring_current < 2.0 or (
                self._params["simul"] and self._params["simul_beamlost"]
            ):

                if self.get_sr_mode() not in [
                    "MDT",
                    "Shutdown",
                ]:
                    if not self._params["simul"]:
                        elog_error("Ring current below 2 mA, injection or beam loss")
                    print(
                        "wait_for_beam: Ring current below 2 mA, injection or beam loss"
                    )
                    do_stabilization_wait = True
                else:
                    return
            else:
                return

            if not self._params["simul"] and fe.is_closed:
                elog_error("wait_for_beam: Front End Closed")

            while (
                self.get_injection_mode() == "Injection"
                or self.get_sr_current() < 2.0
                or (self._params["simul"] and self._params["simul_beamlost"])
            ):
                print("wait_for_beam: still in injection, checking every minute")
                sleep(60)

                if self.get_injection_mode() == "BeamDelivery" and fe.is_closed:
                    print(
                        "wait_for_beam : front end closed, trying to open it every minute"
                    )

                    while fe.is_closed:
                        print(
                            "wait_for_beam : front end closed, trying to open it every minute"
                        )

                        # bliss tango_shutter.open() will raise an exception
                        # if the fe is in STANDBY (i.e "Wait for permission") or FAULT
                        if (
                            fe.state != BaseShutterState.STANDBY
                            and fe.state != BaseShutterState.FAULT
                        ):
                            fe.open()
                            sleep(60)

                    print(
                        "wait_for_beam: front end is now open, the experiment can continue"
                    )

            if do_stabilization_wait and self._params["wfb_stab_time"] != 0:
                print(
                    f"wait_for_beam: waiting {self._params['wfb_stab_time']} seconds for stabilisation\n"
                )
                sleep(self._params["wfb_stab_time"])

    def do_waitforrefill(self, check_time):

        if self._params["wait_for_refill"]:

            self._params["wfr_check_time"] = check_time
            sleep_time = 5
            do_stabilization_wait = False
            refill_duration = 0
            # Here we did optimize nb of calls to machine devices
            # remember this function is called for each chain iteration

            refill = self.get_sr_refill_countdown()

            if refill >= self._params["wfr_check_time"]:
                return
            else:
                if self.get_sr_mode() in ["MDT", "Shutdown"]:
                    return

            while refill < self._params["wfr_check_time"]:

                refill = self.get_sr_refill_countdown()
                if refill == -1:
                    # MDT or Shutdown ==> do not wait
                    break

                if refill > 1:
                    print(
                        f"Waiting for Beam refilling {refill}s ({self._params['wfr_check_time']:g}s required)"
                    )
                else:
                    print(f"Waiting for end of refill (for {refill_duration:d}s) ")
                    sleep_time = 1  # to reduce useless overhead.
                    refill_duration += sleep_time

                sleep(sleep_time)
                do_stabilization_wait = True

            if do_stabilization_wait and self._params["wfr_stab_time"]:
                print(
                    f"Beam is back ! Now waiting stabilization for {self._params['wfr_stab_time']} seconds...\n"
                )
                sleep(self._params["wfr_stab_time"])
