from fscan.mussttools import MusstChanUserCalc


class MusstEnergyCalcChannel(MusstChanUserCalc):
    def __init__(self, name, config):
        self._name = name
        self.mono = config.get("monochromator", None)
        if self.mono is None:
            raise RuntimeError(
                f"MusstEnergyCalcChannel ({name}): No Monochromator specified"
            )
        self.nu_name = self.mono._motors["nu"].name + "_trig"
        self.psi_name = self.mono._motors["psi"].name + "_trig"

    def select_input_channels(self, channels_list, scan_pars):
        return [self.nu_name, self.psi_name]

    def get_output_channels(self):
        return ["energy_enc"]

    def calculate(self, input_data_dict):
        nu_data = input_data_dict[self.nu_name]
        psi_data = input_data_dict[self.psi_name]
        energy_data = self.mono.angles2energy(nu_data, psi_data)
        ene_data = {"energy_enc": energy_data}
        return ene_data
