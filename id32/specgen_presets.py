# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import sleep

from bliss import setup_globals
from bliss.setup_globals import *
from bliss import current_session
from bliss.common.user_status_info import status_message
from bliss.config.settings import HashSetting
from bliss.common.logtools import elog_info, elog_error
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from id32 import specgen_bliss

from bliss.shell import iter_common as _iter_common

import time
import os

class ID32SpecGenChainPreset(ChainPreset):
    def __init__(self, camera, energy):
        self.camera = camera
        self.energy = energy

    def prepare(self, acq_chain):
        self.scan_metadata =  acq_chain.scan.scan_info
        title = acq_chain.scan.scan_info["title"]
        num = acq_chain.scan.scan_number
        self._session = acq_chain.scan.scan_info["session_name"]
        
    def start(self, acq_chain):
        if self._session == "xmcd":
            pass

    def stop(self, acq_chain):
        if self._session == "xmcd":
            pass

    class Iterator(ChainIterationPreset):
        def __init__(self, iteration_nb, preset):
            self.preset = preset
            self.iteration = iteration_nb

        def prepare(self):
            pass

        def start(self):
            pass

        def stop(self):
            if not 'dhyana95v2:image' in ACTIVE_MG.enabled:
                return
            t0 = time.time()
            wait_for_saving = False
            while int(self.iteration) != int(self.preset.camera.proxy.last_image_saved):
                sleep(.001)
                wait_for_saving = True
            if wait_for_saving:
                print("WARNING: waited %d ms for image to be saved" % (1e3*(time.time()-t0)))
            
            
            # ~ print(SCAN_SAVING.data_fullpath)
            # ~ print(self.iteration)
            # ~ print(self.preset.scan_metadata)
            
            specgen_bliss.send_specgen_request(SCAN_SAVING.data_fullpath, self.iteration, self.preset.scan_metadata)
            
            
            
            # ~ directory = self.preset.camera.proxy.saving_directory
            # ~ pref = self.preset.camera.proxy.saving_prefix
            # ~ num_form = self.preset.camera.proxy.saving_index_format
            # ~ suffix = self.preset.camera.proxy.saving_suffix
            # ~ num = self.preset.camera.proxy.last_image_saved
            # ~ filename = f"{directory}/{pref}{num:04d}{suffix}"
            # ~ energy = self.preset.energy.position
            
            
            # ~ print(self.preset.camera.proxy.last_image_saved)
            # ~ print(self.iteration)
            # ~ print(filename)
            # ~ print(energy)

    def get_iterator(self, acq_chain):
        iteration_nb = 0
        while True:
            yield ID32SpecGenChainPreset.Iterator(iteration_nb, self)
            iteration_nb += 1
