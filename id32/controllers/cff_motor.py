# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent, numpy, tabulate, numpy
from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.config.static import get_config

"""
RIXS Grating CFF calculational motor

yml configuration example:

- class: CffMotorController           # object class (inheriting from CalcController)
  plugin: generic                     # CalcController works with generic plugin
  package: id32.controllers.cff_motor # module where the FooCalcController class can be found
  name: rixs_cff_motor_ctrl           # a name for the controller (optional)

  mono_ctrl: $pgm
  axes:
    - name: $mroty
      tags: real nu
    - name: $groty 
      tags: real psi

    - name: cff            # a name for a virtual axis (output)
      tags: cff            # a tag for identification within the controller
"""


class CffMotorController(CalcController):
    def __init__(self, config):
        CalcController.__init__(self, config)
        self._mono_ctrl = config.get("mono_ctrl")

    def calc_from_real(self, real_user_positions):
        """Computes pseudo dial positions from real user positions.

        => pseudo_dial_positions = self.calc_from_real(real_user_positions)

        Args:
            real_user_positions: { real_tag: real_user_positions, ... }
        Return:
            a dict: {pseudo_tag: pseudo_dial_positions, ...}

        """
        _nu = real_user_positions["nu"]
        _psi = real_user_positions["psi"]
        # humm, we should check if the selected grating is a RIXS one, otherwise cff is useless
        _cff = self._mono_ctrl._cff(_nu, _psi)
        return {"cff": _cff}

    def calc_to_real(self, pseudo_dial_positions):
        """Computes reals user positions from pseudo dial positions.

        => real_user_positions = self.calc_to_real(pseudo_dial_positions)

        Args:
            pseudo_dial_positions: {pseudo_tag: pseudo_dial_positions, ...}
        Return:
            a dict: { real_tag: real_user_positions, ... }
        """

        # humm, we should check if the selected grating is a RIXS one, otherwise cff is useless
        _energy = self._mono_ctrl._motors["energy"].position
        if isinstance(pseudo_dial_positions["cff"], numpy.ndarray):
            _nu = []
            _psi = []
            for pos in pseudo_dial_positions["cff"]:
                self._mono_ctrl._grating_sel.cff = pos
                n, p = self._mono_ctrl.energy2angles(_energy)
                _nu.append(n)
                _psi.append(p)
            return {"nu": numpy.array(_nu), "psi": numpy.array(_psi)}
        else:
            self._mono_ctrl._grating_sel.cff = pseudo_dial_positions["cff"]
            _nu, _psi = self._mono_ctrl.energy2angles(_energy)
            return {"nu": _nu, "psi": _psi}
