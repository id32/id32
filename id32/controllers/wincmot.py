# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent, numpy, tabulate
from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.config.static import get_config

"""
XMCD  Window Calculational motor, sample y (sy) using win and frame ones

yml configuration example:

- class: WindowController          # object class (inheriting from CalcController)
  plugin: generic                      # CalcController works with generic plugin
  package: id32.controllers.wincmot # module where the FooCalcController class can be found
  name: multifourc                     # a name for the controller (optional)

  axes:
    - name: $apery
      tags: real win
    - name: $frmy
      tags: real frame

    - name: sy            # a name for a virtual axis (output)
      tags: sy            # a tag for identification within the controller
"""


class WindowController(CalcController):
    def __init__(self, config):
        CalcController.__init__(self, config)
        #self._mode = settings.SimpleSetting(f"{self.name}_mode", default_value="gonio")

    def calc_from_real(self, real_user_positions):
        """Computes pseudo dial positions from real user positions.

        => pseudo_dial_positions = self.calc_from_real(real_user_positions)

        Args:
            real_user_positions: { real_tag: real_user_positions, ... }
        Return:
            a dict: {pseudo_tag: pseudo_dial_positions, ...}

        """
        _frame = real_user_positions["frame"]
        return {"sy": _frame}

    def calc_to_real(self, pseudo_dial_positions):
        """Computes reals user positions from pseudo dial positions.

        => real_user_positions = self.calc_to_real(pseudo_dial_positions)

        Args:
            pseudo_dial_positions: {pseudo_tag: pseudo_dial_positions, ...}
        Return:
            a dict: { real_tag: real_user_positions, ... }
        """

        _sy_pos = pseudo_dial_positions["sy"]
        _last_sy_pos = self._tagged["sy"][0].position
        _diff = _sy_pos - _last_sy_pos
        _frame_pos = self._tagged["frame"][0].position + _diff
        _win_pos = self._tagged["win"][0].position + _diff

        return {"frame": _frame_pos, "win": _win_pos}
