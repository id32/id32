# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import tango
from bliss.common.counter import SoftCounter

"""
USELESS: since we can set allow_failure=False in yml conf for a tango_attr_as_counter counter

A tiny wrapper for tango_attr_as_counter where we manage if the bpm server is in running (On), 
otherwise the attributes are not allowed to be read.
"""


class BpmCounters:

  def __init__(self, tg_device):
    self.proxy= tango.DeviceProxy(tg_device)

  @property
  def x(self):
    if self.proxy.state() == tango.DevState.ON:
      return self.proxy.x
    else:
      return -1
    
  @property
  def fwhm_x(self):
    if self.proxy.state() == tango.DevState.ON:
      return self.proxy.fwhm_x
    else:
      return -1


def create_bpm_softcnt(tg_device):
  """
  Return 2 soft counters x and fwhm_x
  """
  bpmcnt = BpmCounters(tg_device)
  x = SoftCounter(bpmcnt, "x")
  fx = SoftCounter(bpmcnt, "fwhm_x")
  
  return x, fx
    

		
