# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent

from bliss.common.tango import DeviceProxy
from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.utils import autocomplete_property
from bliss.common.counter import SamplingCounter  # noqa: F401
from bliss.controllers.counter import SamplingCounterController
from bliss.common.logtools import log_info, log_debug
from bliss.common.soft_axis import SoftAxis  # noqa: F401
from bliss.common.axis import AxisState
from bliss.controllers.bliss_controller import BlissController
from bliss.config import settings



class SierraIM180Device:
    GAS = {0: "Air", 1: "Argon", 2: "Carbon Dioxide", 3: "Carbon Monoxide", 4: "Helium", 5: "Hydrogen", 6: "Methane", 7: "Nitrogen", 8: " Nitrous Oxide", 9: "Oxygen"}
    
    def __init__(self, config):
        self._config = config
        self._comm = None
        self._offset = config.get("offset", 0.214)
        self._calibration = config.get("calibration", 1.58)

    def __info__(self):
        msg = "Sierra IM-180 Mass-Flow controller\n"
        msg += f"On comm: {self.comm._port}\n"
        msg += f"Gas selected: {self.get_gas_name()}\n"
        msg += f"offset: {self._offset}, calibration: {self._calibration}\n"
        msg += f"setpoint: {self.setpoint}\n"
        msg += f"measure: {self.measure}\n"
        return msg

    @property
    def setpoint(self):
        return self._get_setpoint_flow()
    
    @setpoint.setter
    def setpoint(self, value):
        self._set_setpoint_flow(value)
        
    @property
    def measure(self):
        return self._get_flow()

    def _get_flow(self):
        flow = self._get_raw_flow()
        return flow * self._calibration + self._offset

    def _get_raw_flow(self):
        return self.get_cmd("Flow")

    def _get_setpoint_flow(self):
        sp = self._get_raw_setpoint_flow()        
        return sp * self._calibration + self._offset

    def _get_raw_setpoint_flow(self):
        return self.get_cmd("Setf")

    def _set_setpoint_flow(self, value):
        raw = (value - self._offset)/self._calibration
        self.put_cmd("Setf", raw)
                     
    def get_gas_id(self):
        return int(self.get_cmd("Gasi"))

    def get_gas_name(self):
        id = self.get_gas_id()
        return self.GAS[id]
    
    def set_gas_id(self, value):
        if value <0 or value >9:
            print(f"Wrong Gas Id.{value}, range is [0-9]")
            return
        self.put_cmd("Gasi", value)
        
    @property
    def comm(self):
        if self._comm is None:
            self._comm = get_comm(self._config)
        return self._comm

    def calc_crc(self, cmd):
        crc = 0xffff
         
        lenght = len(cmd);
        for i in range(lenght):
            c = ord(cmd[i])
            crc = crc ^ ((c << 8))

            for j in range(8):
                if(crc & 0x8000) == 0x8000:
                    crc = ((crc<<1) ^ 0x1021) & 0xffff
                else:
                    crc = (crc<<1) & 0xffff;

        c = crc & 0xff00
        if c == 0x0d00 or c == 0x0000:
            crc += 0x0100
        c = crc & 0x00ff
        if c == 0x000d or c == 0x0000:
            crc += 0x0001

        return crc;
        

    def encode(self, cmd):
        crc = self.calc_crc(cmd)
        crcH = (crc >> 8) & 0xff
        crcL = (crc) & 0xff
        s = f"{cmd}{crcH:c}{crcL:c}\r"
        return s

    def decode(self, answer):
        lenght = len(answer)
        if lenght < 4:
            return 0
        #if answer[lenght-1] != "\r":
        #    raise ValueError("cannot decode controller answer, missing \\r")
        crcH = ord(answer[lenght-2])
        crcL = ord(answer[lenght-1])
        cmd = answer[:-2]
        crc = self.calc_crc(cmd)

        if crc != (crcH <<8) | crcL:
            raise ValueError("Cannot decode controller answer, wrong CRC")
        return cmd

    def put_cmd(self, cmd, param):

        cmd = f"!{cmd}{param}"
        encoding = self.encode(cmd)
        self.comm.write(encoding.encode('latin-1'))
        gevent.sleep(0.3)
        
    def get_cmd(self, cmd):
        cmd = f"?{cmd}"
        encoding = self.encode(cmd)
        self.comm.write(encoding.encode('latin-1'))
        encoded = self.comm.readline()
        ans = self.decode(encoded.decode('latin-1'))
        lenght = len(cmd)
        return float(ans[lenght-1:])

    def get_sync(self):
        self._gas = {}
        cmd = "?Sync"
        encoding = self.encode(cmd)
        self.comm.write(encoding.encode('latin-1'))
        encoded = self.comm.readline()
        ans = self.decode(encoded.decode('latin-1'))
        while ans != "Sync":
            if ans.startswith("Gasn"):
                n = int(ans[4])
                gas = ans[5:]
                self._gas[n] = gas
                print (f"Gas n. {n} is {gas}")
            elif ans.startswith("Gasi"):
                n = int(ans[4])
                print (f"Selected Gas (id.{ans[4]}) is {self.GAS[n]}")
            else:
                print(ans)
                
            encoded = self.comm.readline()
            ans = self.decode(encoded.decode('latin-1'))            
        
        
