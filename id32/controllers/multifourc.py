# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent, numpy, tabulate
from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.config.static import get_config
from bliss.shell.getval import getval_name

"""
RIXS Calculational motor  to change tth motor in a fourc geometry

yml configuration example:

- class: MultiFourcController          # object class (inheriting from CalcController)
  plugin: generic                      # CalcController works with generic plugin
  package: id32.controllers.multifourc # module where the FooCalcController class can be found
  name: multifourc                     # a name for the controller (optional)

  axes:
    - name: $dtth
      tags: real gonio
    - name: $rtth
      tags: real spectro

    - name: tth            # a name for a virtual axis (output)
      tags: tth            # a tag for identification within the controller
"""


class MultiFourcController(CalcController):
    def __init__(self, config):
        CalcController.__init__(self, config)
        self._mode = settings.SimpleSetting(f"{self.name}_mode", default_value="gonio")

    def calc_from_real(self, real_user_positions):
        """Computes pseudo dial positions from real user positions.

        => pseudo_dial_positions = self.calc_from_real(real_user_positions)

        Args:
            real_user_positions: { real_tag: real_user_positions, ... }
        Return:
            a dict: {pseudo_tag: pseudo_dial_positions, ...}

        """
        _gonio = real_user_positions["gonio"]
        _spectro = real_user_positions["spectro"]
        if self._mode.get() == "gonio":
            _tth = _gonio
        else:
            _tth = _spectro
        return {"tth": _tth}

    def calc_to_real(self, pseudo_dial_positions):
        """Computes reals user positions from pseudo dial positions.

        => real_user_positions = self.calc_to_real(pseudo_dial_positions)

        Args:
            pseudo_dial_positions: {pseudo_tag: pseudo_dial_positions, ...}
        Return:
            a dict: { real_tag: real_user_positions, ... }
        """

        _tth_pos = pseudo_dial_positions["tth"]
        if self._mode.get() == "gonio":
            return {"gonio": _tth_pos}
        else:
            return {"spectro": _tth_pos}

    def fourc_mode(self, mode=None):
        _curr_mode = self._mode.get()
        _tth = self._tagged["tth"][0]
        _real_tth = self._tagged[_curr_mode][0]

        if mode != "gonio" and mode != "spectro":
            msg = f"Fourc tth mode is {_curr_mode}, {_tth.name} -> {_real_tth.name}"
            print(msg)
            mode = getval_name("Change fourc tth mode [gonio/spectro]", _curr_mode)
            if not mode:
                mode = _curr_mode
        self._mode.set(mode)

        _real_tth = self._tagged[mode][0]

        self.sync_hard()
        _tth.limits = _real_tth.limits
        msg = (
            f"Fourc mode choosen is {self._mode.get()}, {_tth.name} -> {_real_tth.name}"
        )
        print(msg)
