# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent

from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.utils import autocomplete_property
from bliss.common.counter import SamplingCounter  # noqa: F401
from bliss.controllers.counter import SamplingCounterController
from bliss.common.logtools import log_info, log_debug
from bliss.common.soft_axis import SoftAxis  # noqa: F401
from bliss.common.axis import AxisState
from bliss.controllers.bliss_controller import BlissController
from bliss.config import settings

"""
Helium Needle valve controller for cryogenic magnet, acessible via RS232

The Needle Valve Controller is designed to interface to a stepper motor driven needle
valve which will allow the helium flow rate to be controlled by a computer.
The connections available include a connector for either USB or RS232
communication. An LCD display provides information on the operation of
the controller to the user.

 
yml configuration example:

- class: NeedleValveController
  #module: powersupply.cryogenic_needle_valve
  package: id32.controllers.cryogenic_needle_valve
  plugin: generic
  name: nvalve_ctrl
  serial:
    url: tango://id32/Serialrp_232/lid323_ttyR34
    baudrate: 9600
    eol: "\r\n"
     
   axis:
     - name: nvalve
       tolerance: 0.001
       steps_per_unit: -4000
       encoder_step_size: 9.6

   counters:
     - name: nvpress
       tag: pressure
       unit: mBar
"""


class NeedleValveCC(SamplingCounterController):
    def __init__(self, name, ctrl):
        super().__init__(name)
        self.ctrl = ctrl

    def read(self, counter):
        return getattr(self.ctrl, counter.tag)


class NeedleValveController(BlissController):
    def __init__(self, config):
        super().__init__(config)
        self._device = NeedlValveDevice(config)
        self._scc = NeedleValveCC(self.name, self._device)
        global_map.register(self, parent_list=["controllers", "counters", "axes"])

    def __info__(self, show_module_info=True):
        info_list = []
        info_list.append(f"\nComm: {self.device.comm.__info__()}")

        # status = self._device.update

        return "\n".join(info_list)

    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "counters":
            return "SamplingCounter"

        elif parent_key == "axis":
            return "SoftAxis"

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        if parent_key == "counters":
            mode = cfg.get("mode", "MEAN")
            unit = cfg.get("unit", "")
            obj = self._scc.create_counter(item_class, name, mode=mode, unit=unit)
            obj.tag = cfg["tag"]
            return obj
        elif parent_key == "axis":
            self.device.steps_per_unit = cfg.get("steps_per_unit")
            self.device.encoder_step_size = cfg.get("encoder_step_size")
            return item_class(
                name,
                self,
                position=self._axis_position,
                move=self._axis_move,
                stop=self._axis_stop,
                state=self._axis_state,
                low_limit=20,
                high_limit=234,
                tolerance=cfg.get("tolerance", 0.1),
                unit=cfg.get("unit", ""),
                export_to_session=False,
                as_positioner=True,
            )

    def _load_config(self):
        for cfg in self.config["counters"]:
            self._get_subitem(cfg["name"])

        self._nvalve_axis = self._get_subitem(self.config["axis"][0]["name"])

    def _axis_position(self):
        """Return the actual field of the power supply as the current position of the associated soft axis"""
        return self.device.position

    def _axis_move(self, pos):
        """Set the target field of the power supply a new value of the associated soft axis"""
        self.device.amove(pos)

    def _axis_stop(self):
        """Stop the motion of the associated soft axis"""
        self.device.stop()

    def _axis_state(self):
        """Return the current state of the associated soft axis."""
        # Standard axis states:
        # MOVING : 'Axis is moving'
        # READY  : 'Axis is ready to be moved (not moving ?)'
        # FAULT  : 'Error from controller'
        # LIMPOS : 'Device high limit active'
        # LIMNEG : 'Device low limit active'
        # HOME   : 'Home signal active'
        # OFF    : 'Axis is disabled (must be enabled to move (not ready ?))'

        # for the moment, amove a blocking call since the controller send status in continuous
        # one should spawn the command which could update the status....
        return AxisState("READY")

        if self.device.moving_state == "moving":
            return AxisState("MOVING")
        else:
            return AxisState("READY")

    @autocomplete_property
    def counters(self):
        return self._scc.counters

    @autocomplete_property
    def device(self):
        return self._device

    @property
    def pressure(self):
        return self.device.pressure


class NeedlValveDevice:
    def __init__(self, config):
        self._config = config
        self._comm = None
        self._timeout = config.get("timeout", 3.0)
        self.__channel = None

        self.__steps_per_unit = 1
        self.__encoder_step_size = 9.6

        self.verbose = False

    @property
    def comm(self):
        if self._comm is None:
            self._comm = get_comm(self._config)
        return self._comm

    @property
    def steps_per_unit(self):
        return self.__steps_per_unit

    @steps_per_unit.setter
    def steps_per_unit(self, val):
        self.__steps_per_unit = val

    @property
    def encoder_step_size(self):
        return self.__encoder_step_size

    @steps_per_unit.setter
    def encoder_step_size(self, val):
        self.__encoder_step_size = val

    @property
    def position(self):
        """Get the current position in user unit (mm)"""
        steps = int(self.send_cmd("GN"))
        pos_mm = steps / self.steps_per_unit
        return pos_mm

    def amove(self, pos):
        """
        Absolute move in user unit (mm)
        but controller M  cmd is a relative move
        """
        curr_steps = int(self.send_cmd("GN"))
        steps = pos * self.steps_per_unit
        # ----- quantification of 2 encoder steps!
        _diff = -(steps - curr_steps)
        #diffQ = 2 * int(abs(_diff) / 2.0) * self.encoder_step_size
        diffQ = 2 * int(abs(_diff) / 2.0) * 9.6
        _diff = diffQ if _diff > 0 else -diffQ

        sign = "+" if _diff > 0 else "-"
        incr = int(abs(_diff)+0.5)
        self.send_cmd(f"*M{incr}{sign}", nb_lines=0)

        while True:
            gevent.sleep(0.3)
            p = self.comm.raw_read().decode()
            if p.find("disabled") != -1:
                break

    @property
    def moving_state(self):
        """Return the interna moving status, moving or stationary"""
        st = self.send_cmd("M")
        if st == "moving":
            return "moving"
        else:  # stationary
            return "ready"

    def stop(self):
        """Emergency stop"""
        self.send_cmd("M0")

    @property
    def pressure(self):
        """Get pressure in millibar using the current gain"""
        resp = self.send_cmd("GPM")
        return float(resp.split()[0])

    @property
    def verbose(self):
        return self.__verbose

    @verbose.setter
    def verbose(self, flag):
        """Switch on or off the verbose mode of the device"""
        
        # switching off the verbose set mode to the so-called labVIEW mode
        # in that case the GET commands just return value 
        # For instance the GET position command "GN" returns in verbose mode "Position=98" and in non-verbose (labVIEW)  "98" .

        # WARNING: the controller onyl rely on non-verbose mode. 
        mode = 0 if flag else 1
        self.send_cmd(f"SV{mode}")
        self.__verbose = flag

    def send_cmd(self, cmd, nb_lines=2):
        """
        Send a command to the controller
        if nb_lines is 0, just write, not answer is expected
        """
        cmd = cmd + "\n"
        if nb_lines != 0:
            asw = self.comm.write_readlines(cmd.encode(), nb_lines=nb_lines)
            self.comm.raw_read()
            return asw[1].decode()
        else:
            self.comm.write(cmd.encode())
