# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


from bliss.config import settings
from bliss.config.beacon_object import BeaconObject
from bliss.shell import getval
from bliss.common.logtools import elog_info
import time, gevent

class HeBalance(BeaconObject):

    def __init__(self, name, config):
        BeaconObject.__init__(self, config)

        self._balance = config.get("balance", None)
        if self._balance is None:
            raise ValueError("balance is missing in yml config")
        self.__bottle_weight = None
        self.__tline_weight = None
        self._check_loop_task = None
        
        def_calc = {"start_time": 0.0, "new_weight": 0.0, "start_weight": 0.0, "old_weight":0.0, "time_last_measure": 0.0, "liters_left": 0.0, " time_left_hrs": 0.0, "average_rate": 0.0, "current_rate": 0.0, "check_delay": 6.0}
        self.__calc = settings.HashSetting(f"{self.name}_calc", default_values= def_calc)
        
    def __info__(self):
        info = f"He balance:\n  - Empty bottle weight {self.bottle_weight} kg\n  - Transferline weight {self.transferline_weight} kg\n"
        info += f"  - Current weight {self._balance.weight('raw')} kg"
        return info
    
    @BeaconObject.property(must_be_in_config=True)
    def bottle_weight(self):
        return self.__bottle_weight

    @bottle_weight.setter
    def bottle_weight(self, value):
        if value <= 40:
            raise ValueError("Invalid bottle weight, using average weight of 50.6 kg")
        self.__bottle_weight = value

    @BeaconObject.property(must_be_in_config=True)
    def transferline_weight(self):
        return self.__tline_weight

    @transferline_weight.setter
    def transferline_weight(self, value):
        self.__transferline_weight = value
    
    def new_bottle(self):
        self.bottle_weight = getval.getval_float("Weight of the empty bottle in kg", default=self.bottle_weight)
        self.start_weight = getval.getval_float("Starting weight in kg (measured)", default=self.balance.weight("raw"))
        self.transferline_weight = getval.getval_float("Weight of the transfer line in kg", default=self.transferline_weight)
        self.start_time = time.time()
        if getval.getval_yes_no(f"Start time is NOW, change it ?", default=False):            
            self.start_time = getval.getval_float("New start time in epoch", default=time.time())

        self.__calc["start_weight"] = self.start_weight
        self.__calc["old_weight"] = self.start_weight

    def measure(self, verbose=True):

        msg = ""
        old_weight = self.__calc["old_weight"]
        new_weight = self.balance.weight("raw")
        time_meas = time.time()
        self.__calc["new_weight"] = new_weight

        start_weight = self.__calc["start_weight"]
        empty_bottle_weight = self.bottle_weight + self.transferline_weight
        remain_weight = new_weight - empty_bottle_weight
        remain_liters = remain_weight/0.125

        cons_weight = old_weight - new_weight
        time_last = self.__calc["time_last_measure"]
        time_between = (time_meas - time_last)/3600
        time_start = self.__calc["start_time"]
        time_since_start = (time_meas - time_start)/3600
        diff_in_weight = start_weight - new_weight
        cons_since_start = diff_in_weight/0.125

        if cons_since_start == 0:
            since_start_rate = 1/24. #estimated boiloff rate 1l/day
        else:
            since_start_rate = cons_since_start/time_since_start
        if cons_weight == 0:
            cons_rate = 1/24. #estimated boiloff rate 1 l/day
        else:
            cons_rate = cons_weight/0.125/time_between

        if cons_weight == 0:
            msg ="No consumption since last time measured, please try later\n"
            time_left = remain_liters/since_start_rate
        else:
            time_left = remain_liters/cons_rate

        msg += f"Starting weight: {start_weight:4.2f} kg\n"
        msg += f"New He weight: {new_weight:5.2f} kg\n"
        msg += f"Estimated {remain_liters:4.1f} liters in bottle\n"
        msg += f"Consumption since starting: {diff_in_weight:4.2f} kg ({cons_since_start:4.1f} l)\n"
        msg += f"Average rate since starting: {since_start_rate:4.2f} l/hr\n"
        if cons_rate == 0:
            msg += f"Cannot determine current rate, {time_left:4.1f} hours left at average rate since starting\n"
        else:
            msg += f"{time_left:4.1f} hours left of liquid Helium at rate of {cons_rate:4.2f} l/hr\n"

        if time_left <= 12:
            msg += "WARNING: less than 12 hrs of Helium left, consider changing the bottle"


        self.__calc["old_weight"] = new_weight
        self.__calc["time_last_measure"] = time_meas
        self.__calc["liters_left"] = remain_liters
        self.__calc["time_left_hrs"] = time_left
        self.__calc["average_rate"] = since_start_rate
        self.__calc["current_rate"] = cons_rate
        
        print (msg)
        elog_info(msg)
        
    @property
    def start_time(self):
        return self.__calc["start_time"]

    @property
    def start_time_as_str(self):
        return time.ctime(self.start_time)

    @start_time.setter
    def start_time(self, value):
        self.__calc["start_time"] = value

    @property
    def start_weight(self):
        return self.__calc["starting_weight"]

    @start_weight.setter
    def start_weight(self, value):
        self.__calc["starting_weight"] = value        
    


    def _check_loop(self):
        while True:
            chk_time = time.time() - self.__calc["time_last_measure"]
            chk_delay = self.__calc["check_delay"]*3600
            if chk_time > chk_delay: self.measure(verbose=True)


    def check_on(self, delay=6):
        self.__calc["check_delay"] = delay

        print(f"======> Automatically checking Helium level every {delay} hours\n")
        if self._check_loop_task is not None:
            self._check_loop_task.kill()
        self._check_loop_task = gevent.spawn(self._check_loop)


    def check_off(self):
        if self._check_loop_task is not None:
            self._check_loop_task.kill()
        print("======> Automatic helium check off")
