from bliss.controllers.regulation.tango_ds import TangoInput


#
# Just a helper class to embed some specific lakeshore336 input
# commands.
#
class LakeShore336TangoInput(TangoInput):
    def __init__(self, config):

        super().__init__(config)

    def sensor_off(self):
        self.device.target_call("sensor_off")

    def sensor_on(self):
        self.device.target_call("sensor_on")
