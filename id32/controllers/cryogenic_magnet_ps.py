# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent

from bliss.common.tango import DeviceProxy
from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.utils import autocomplete_property
from bliss.common.counter import SamplingCounter  # noqa: F401
from bliss.controllers.counter import SamplingCounterController
from bliss.common.logtools import log_info, log_debug
from bliss.common.soft_axis import SoftAxis  # noqa: F401
from bliss.common.axis import AxisState
from bliss.controllers.bliss_controller import BlissController
from bliss.config import settings

import time

"""
Cryogenic Intelligent Reverse power supply 
for cryogenic 9T magnet, acessible via RS232
 
yml configuration example:

- class: CryogenicPSController
  package: id32.controllers.cryogenic_magnet_ps
  plugin: generic
  name: mps
  tango_name: id32/cryogenic_magnet_ps/xmcd
  serial:
    url: tango://id32/Serialrp_232/lid323_ttyR33
    baudrate: 9600
    eol: "\r\n"
     
  coils:
    - channel: 0
      name: coil9T
      max_field: 9.0203
      volt_limit: 19.
      tesla_per_amper: 0.04909
    - channel: 1
      name: coil4T
      max_field: 4.0504
      volt_limit: 18.
      tesla_per_amper: 0.02094
               
   field_axis:
     - name: magnet
       tolerance: 0.001
       steps_per_unit: 1
       sweep_time: 20.

   counters:
     - name: target_field
       tag: target_field
     - name: magnet_field
       tag: magnet_field
"""


class CryogenicPSCC(SamplingCounterController):
    def __init__(self, name, mps):
        super().__init__(name)
        self.mps = mps

    def read(self, counter):
        return getattr(self.mps, counter.tag)


class CryogenicPSController(BlissController):
    def __init__(self, config):
        super().__init__(config)

        yml_tango = config.get("tango", None)
        tg_device = yml_tango.get("url", None)
        if tg_device is not None:
            self._is_tango = True
            self._device = DeviceProxy(tg_device)
        else:
            self._is_tango = False
            self._device = CryogenicPSDevice(config)

        self._scc = CryogenicPSCC(self.name, self._device)
        global_map.register(self, parent_list=["controllers", "counters", "axes"])
        self._to_end = True

    def __info__(self, show_module_info=True):
        info_list = []
        if self._is_tango:
            info_list.append(f"\nComm: {self.device}")
        else:
            info_list.append(f"\nComm: {self.device.comm.__info__()}")
        # Get full identification string
        status = self._device.update

        info_list.append("STATUS:")
        for st in status:
            info_list.append(f"  - {st}")

        info_list.append(f"\n{self._get_coils()}")
        info_list.append(f"  - ramp rate: {self.ramp_rate:.4g} T/mn")

        return "\n".join(info_list)

    def _get_coil_params(self, channel):
        for coil in self.config.get("coils"):
            if coil.get("channel") == channel:
                return coil.to_dict()
        raise ValueError(
            f"Coil channel {channel} is not configured, check the YML file"
        )

    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "counters":
            return "SamplingCounter"

        elif parent_key == "field_axis":
            return "SoftAxis"

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        if parent_key == "counters":
            mode = cfg.get("mode", "MEAN")
            unit = cfg.get("unit", "")
            obj = self._scc.create_counter(item_class, name, mode=mode, unit=unit)
            obj.tag = cfg["tag"]
            return obj
        elif parent_key == "field_axis":
            self._sweep_time = cfg.get("sweep_time", 20)
            active_channel = self.device.channel
            max_field = self._get_coil_params(active_channel)["max_field"]
            return item_class(
                name,
                self,
                position=self._axis_position,
                move=self._axis_move,
                stop=self._axis_stop,
                state=self._axis_state,
                low_limit=-max_field,
                high_limit=max_field,
                tolerance=cfg.get("tolerance", 0.1),
                unit="T",
                export_to_session=False,
                as_positioner=True,
            )

    def _load_config(self):
        for cfg in self.config["counters"]:
            self._get_subitem(cfg["name"])

        self._field_axis = self._get_subitem(self.config["field_axis"][0]["name"])

    def _axis_position(self):
        """Return the actual field of the power supply as the current position of the associated soft axis"""
        return self.magnet_field

    def _axis_move(self, pos):
        """Set the target field of the power supply a new value of the associated soft axis"""
        # for a new motion in reverse direction one must first ramp the field to 0
        # then move to the new field position
        # the second move will be run from the _axis_state() function
        self._positive_signed_end = pos >= 0
        current_sign_is_positive = self.device.direction_positive

        self.device.pause = True
        abs_pos = abs(pos)
        self.device.target_field = abs_pos
        # in case of reverse magnet sign, ramp to 0 first here
        if self._positive_signed_end == current_sign_is_positive:
            self.device.ramp_to_target()
            self._to_end = True
        else:
            if self.magnet_field == 0:
                self.device.direction_positive = self._positive_signed_end
                self.device.ramp_to_target()
                self._to_end = True
            else:
                self.device.ramp_to_zero()
                gevent.sleep(1)
                self._to_end = False        

        self.device.pause = False

    def _axis_stop(self):
        """Stop the motion of the associated soft axis"""
        self._to_end = True
        self.device.send_cmd("pause on")

    def _axis_state(self):
        """Return the current state of the associated soft axis."""
        # Standard axis states:
        # MOVING : 'Axis is moving'
        # READY  : 'Axis is ready to be moved (not moving ?)'
        # FAULT  : 'Error from controller'
        # LIMPOS : 'Device high limit active'
        # LIMNEG : 'Device low limit active'
        # HOME   : 'Home signal active'
        # OFF    : 'Axis is disabled (must be enabled to move (not ready ?))'

        r_status = self.device.ramping_status
        if r_status == "ramping":
            return AxisState("MOVING")
        elif r_status == "holding":
            stat = AxisState("READY")

            # manage here the end of the field sign reversing
            if self._to_end:
                return stat
            elif self.magnet_field == 0:
                self._to_end = True
                # wait at zero field before continuing sweep
                gevent.sleep(self._sweep_time)
                # change the direction
                self.device.direction_positive = self._positive_signed_end
                # ramp to the final field in the new direction
                self.device.ramp_to_target()
                gevent.sleep(1)
                return AxisState("MOVING")
            else:
                return stat
        else:
            return AxisState("FAULT")

    @autocomplete_property
    def counters(self):
        return self._scc.counters

    @autocomplete_property
    def device(self):
        return self._device

    @property
    def target_field(self):
        """Field in T"""
        return self.device.target_field

    @target_field.setter
    def target_field(self, value):
        """Target field in T"""
        self.device.target_field = value

    @property
    def ramp_rate(self):
        """Target ramp rate in T/min"""
        return self.device.ramp_rate

    @ramp_rate.setter
    def ramp_rate(self, value):
        """Target ramp rate in T/min"""
        self.device.ramp_rate = value

    @property
    def magnet_field(self):
        """Persistent field in T"""
        return self.device.magnet_field

    @property
    def status(self):
        """Reads the verbose updated device status"""
        return self.device.update

    @property
    def coils(self):
        print(self._get_coils())

    def _get_coils(self):
        ch = self.device.channel
        coil = self._get_coil_params(ch)["label"]
        return f"ACTIVE CHANNEL: {ch} --> {coil}"

    @coils.setter
    def coils(self, ch):
        if ch != 0 and ch != 1:
            raise ValueError("Wrong channel number, please use 0 or 1")
        params = self._get_coil_params(ch)
        self._change_coil(params)
        # then change the magnet limits
        self._field_axis.limits = (-params["max_field"], params["max_field"])

    def _change_coil(self, params):
        """procedure to change the coil channel"""
        channel = params["channel"]
        self.device.channel = channel
        # just read the heater status
        asw = self.device.send_cmd("heater")
        print(asw[0])
        asw = self.device.send_cmd("ch")
        print(asw[0])
        # select the power-supply output reading in Tesla rather than in Ampere
        asw = self.device.send_cmd("tesla on")
        print(asw[0])
        # set the target to 0 Tesla
        asw = self.device.send_cmd("set mid 0.0")
        print(asw[0])
        # set the constant tesla-per-ampere
        tpa = params["tesla_per_amper"]
        asw = self.device.send_cmd(f"set tpa {tpa}")
        print(asw[0])
        # set the voltage limit
        lim = params["volt_limit"]
        asw = self.device.send_cmd(f"set limit {lim}")
        print(asw[0])
        # set heater to 0
        asw = self.device.send_cmd("set heater 0.0")
        print(asw[0])
        # set max field
        mf = params["max_field"]
        asw = self.device.send_cmd(f"set max {mf}")
        print(asw[0])
        # get the direction
        asw = self.device.send_cmd("get sign")
        print(asw[0])
        # switch off the external trip facility
        asw = self.device.send_cmd("xtrip off")
        print(asw[0])
        # set now the ramp rate, is config one if not been set yet

        rr = (
            params["ramp_rate"]
            if self.device.get_channel_ramp_rate(channel) == -1
            else self.device.get_channel_ramp_rate(channel)
        )
        self.device.ramp_rate = rr
        # read the ramp rate
        asw = self.device.send_cmd("set ramp")
        print(f"{asw[0]} --> {self.device.ramp_rate:.4g} T/mn")
        # get the output voltage
        asw = self.device.send_cmd("get ouput")
        print(asw[0])


class CryogenicPSDevice:

    # Voltage output calibration done by Cryogenics x correction
    # based on our measurements (ID32)
    _output_volt_to_ampere = 1000 / (25.041667 * 0.998)

    def __init__(self, config):
        self._config = config
        self._comm = None
        self._timeout = config.get("timeout", 3.0)
        self.__channel = None

        self._ramp_rates = settings.HashSetting(
            "MpsRampRates", default_values={"0": -1, "1": -1}
        )

    @property
    def comm(self):
        if self._comm is None:
            self._comm = get_comm(self._config)
        return self._comm

    def get_channel_ramp_rate(self, channel):
        return self._ramp_rates[channel]

    @property
    def magnet_field(self):
        """Field in T"""
        return self.read_float("get output", 2)

    @property
    def max_field(self):
        """maximum field in T"""
        return self.read_float("get max", 4)

    @property
    def voltage_limit(self):
        """in Volt"""
        return self.read_float("get vl", 4)

    @property
    def field_constant(self):
        """in Telsa per Ampere"""
        return self.read_float("get tpa", 4)

    @property
    def target_field(self):
        """target in T"""
        tf = self.read_float("get mid", 4)
        sign = 1 if self.direction_positive else -1
        return tf * sign

    @target_field.setter
    def target_field(self, value):
        """Set the target value field in T"""
        if abs(value) > self.max_field:
            raise ValueError(f"target field out of range [-/+ {self.max_field}]")
        asw = self.send_cmd(f"set mid {value}")

    @property
    def pause(self):
        asw = self.send_cmd("pause")[0]
        if asw.find("off") != -1:
            return False
        else:
            return True

    @pause.setter
    def pause(self, flag: bool):
        p = "on" if flag else "off"
        self.send_cmd(f"pause {p}")

    def ramp_to_zero(self):
        self.send_cmd("ramp 0", 0)

    def ramp_to_target(self):
        self.send_cmd("ramp mid", 0)

    def ramp_to_max(self):
        self.send_cmd("ramp max", 0)

    @property
    def direction_positive(self):
        asw = self.send_cmd("get sign")[0]
        dir = True if asw.find("positive") != -1 else False
        return dir

    @direction_positive.setter
    def direction_positive(self, flag: bool):
        dir = "+" if flag else "-"
        asw = self.send_cmd(f"direction {dir}", 0)

    @property
    def ramp_rate(self):
        """Target ramp rate in T/min"""

        self._field_const = self.read_float("get tpa")  # in Tesla/Ampere
        self._current_rate = self.read_float("get rate")  # in Ampere/Second
        rr = self._current_rate * self._field_const * 60
        if self.__channel is None:
            self.__channel = self.channel
        self._ramp_rates[self.__channel] = rr
        return rr

    @ramp_rate.setter
    def ramp_rate(self, value):
        """Set the value ramp rate in T/min"""

        self._field_const = self.read_float("get tpa")  # in Telsa/Ampere
        self._current_rate = value / (self._field_const * 60)
        applied_rate = self.read_float(f"set ramp {self._current_rate}", pos=3)
        if self.__channel is None:
            self.__channel = self.channel
        self._ramp_rates[self.__channel] = value

        log_debug(self, f"applied ramp rate = {applied_rate}")

    @property
    def channel(self):
        return int(self.send_cmd("ch")[0].split()[1])

    @channel.setter
    def channel(self, channel):
        # channel 0: 9T, channel 1: 4T
        if channel != 0 and channel != 1:
            raise ValueError(f"Invalid channel number {channel}, choose 0 or 1.")
        if self.magnet_field != 0.0:
            raise ValueError("Move magnet to zero field first !!!")

        asw = self.send_cmd(f"ch {channel}")
        self.__channel = channel

    @property
    def ramping_status(self):
        asw = self.send_cmd("ramp status")
        # attended answers:
        # "RAMP STATUS: HOLDING ON"
        # "RAMP STATUS: RAMPING FROM"
        if asw[0].find("ramping") != -1:
            return "ramping"
        elif asw[0].find("holding") != -1:
            return "holding"
        else:
            # should never happen
            raise Exception("Cannot interpret this ramping status: {asw[0]}")

    @property
    def update(self):
        """the updated status"""
        stat = self.send_cmd("update", 13)
        return stat

    def read_float(self, cmd, pos=4):
        asw = self.send_cmd(cmd, 1)[0]
        ret = float(asw.split()[pos])
        return ret

    def send_cmd(self, cmd, nb_lines=1):
        """
        Send a command to the controller
        if nb_lines is 0, just write, not answer is expected
        """
        cmd = cmd + "\n"
        if nb_lines != 0:
            asw = self.comm.write_readlines(cmd.encode(), nb_lines=nb_lines)
            ret = [l.decode().strip(".").lower()[1:] for l in asw]
            return ret
        else:
            self.comm.write(cmd.encode())
