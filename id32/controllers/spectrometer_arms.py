# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent, numpy, tabulate

from bliss.shell.getval import getval_int, getval_float, getval_name
from bliss.common.tango import DeviceProxy
from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.utils import autocomplete_property
from bliss.common.logtools import log_info, log_debug
from bliss.common.axis import AxisState
from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.config.beacon_object import BeaconObject
from bliss.config.static import get_config
from bliss import setup_globals

"""
RIXS or XMCD Spectrometer Arms with grating mirror CalcMotor controller

yml configuration example:

- class: SpectrometerArmsController                    # object class (inheriting from CalcController)
  plugin: generic                                      # CalcController works with generic plugin
  package: package: id32.controllers.spectrometer_arms # module where the FooCalcController class can be found
  name: rixs_arms_grating                              # a name for the controller (optional)

  axes:
    - name: $detx         # a reference to a real axis (input)
      tags: real detx     # tags for identification within the controller
    - name: $detz
      tags: real detz
    - name: $grtx 
      tags: real grtx

    - name: r1             # a name for a virtual axis (output)
      tags: r1             # a tag for identification within the controller
    - name: r2             # a name for a virtual axis (output)
      tags: r2             # a tag for identification within the controller

  grating_modes:
    - name: RIXS_2500
      a0: 2501.6
      a1: 0.2963
      a2: -4.72e-5
      radius: 122000
      axis_name: gr2roty
      offset: 0
    - name: RIXS_1400
      a0: 1399.05
      a1: 0.157
      a2: -5.49e-5
      radius: 122000
      axis_name: gr1roty
      offset: 0 
  grating_offsets:
    - grtx: 0
      detx: 0
      detz: 0
"""


class SpectrometerArmsController(CalcController):
    def __init__(self, config):
        CalcController.__init__(self, config)

        self._grating_modes = {}
        for gr_mode in self.config.get("grating_modes", {}):
            name = gr_mode["name"]
            self._grating_modes[name] = settings.HashSetting(
                f"{self.name}_{name}", default_values=gr_mode
            )

        gr_offset = self.config.get("grating_offsets", {})[0]
        self._grating_offsets = settings.HashSetting(
            f"{self.name}_grating_offsets", default_values=gr_offset
        )

        def_grating = list(self._grating_modes.keys())[0]
        self._grating_selected = settings.SimpleSetting(
            f"{self.name}_grating_selected", default_value=def_grating
        )

    def __info__(self):
        grating = self.grating_selected
        info = f"{self.name}: Spectrometer Calc motors:\n"
        info += f"  - detx = {self._tagged['detx'][0].name}, detz = {self._tagged['detz'][0].name}, grtx = {self._tagged['grtx'][0].name}\n"
        info += f"  - r1 = {self._tagged['r1'][0].name}, r2 = {self._tagged['r2'][0].name}\n"
        info += f"  - Grating mode = {grating}:\n"
        info += f"      - a0             = {self._grating_modes[grating]['a0']}\n"
        info += f"      - a1             = {self._grating_modes[grating]['a1']}\n"
        info += f"      - a2             = {self._grating_modes[grating]['a2']}\n"
        info += f"      - radius         = {self._grating_modes[grating]['radius']}\n"
        info += (
            f"      - grating motor  = {self._grating_modes[grating]['axis_name']}\n"
        )
        info += f"      - grating offset = {self._grating_modes[grating]['offset']}\n"

        info += f"      - grtx offset = {self._grating_offsets['grtx']}\n"
        info += f"      - detx offset = {self._grating_offsets['detx']}\n"
        info += f"      - detz offset = {self._grating_offsets['detz']}\n"
        return info

    def calc_from_real(self, real_user_positions):
        """Computes pseudo dial positions from real user positions.

        => pseudo_dial_positions = self.calc_from_real(real_user_positions)

        Args:
            real_user_positions: { real_tag: real_user_positions, ... }
        Return:
            a dict: {pseudo_tag: pseudo_dial_positions, ...}
        """
        r1_pos = real_user_positions["grtx"]
        r2_pos = numpy.sqrt(
            numpy.power(real_user_positions["detz"], 2)
            + numpy.power(real_user_positions["detx"] - real_user_positions["grtx"], 2)
        )

        return {"r1": r1_pos, "r2": r2_pos}

    def calc_to_real(self, pseudo_dial_positions):
        """Computes reals user positions from pseudo dial positions.

        => real_user_positions = self.calc_to_real(pseudo_dial_positions)

        Args:
            pseudo_dial_positions: {pseudo_tag: pseudo_dial_positions, ...}
        Return:
            a dict: { real_tag: real_user_positions, ... }
        """
        beta = self.beta

        detx_pos = (
            pseudo_dial_positions["r2"] * numpy.cos(beta)
            + self._tagged["grtx"][0].position
        )
        detz_pos = pseudo_dial_positions["r2"] * numpy.sin(beta)
        grtx_pos = pseudo_dial_positions["r1"]

        return {"detx": detx_pos, "detz": detz_pos, "grtx": grtx_pos}

    @property
    def beta(self):
        """
        The Beta angle in radian
        """
        detx_pos = self._tagged["detx"][0].position
        detz_pos = self._tagged["detz"][0].position
        grtx_pos = self._tagged["grtx"][0].position
        return numpy.arctan(detz_pos / (detx_pos - grtx_pos))

    def grating_set(self, grating=None):
        if grating is None:
            print("Available Grating:")
            for gr in self._grating_modes:
                print(f"    {gr}\n")
            return
        else:
            if grating not in self._grating_modes:
                print(f"Grating {grating} is not defined")
                print("Available Grating:")
                for gr in self._grating_modes:
                    print(f"    {gr}\n")
                return
        self._grating_selected.set(grating)

    @property
    def grating_selected(self):
        return self._grating_selected.get()

    def grating_set_param(self, grating=None):
        """ """
        if grating is None or grating not in self._grating_modes:
            print(f"Grating {grating} is not defined")
            print("Available Grating:")
            for gr in self._grating_modes:
                print(f"    {gr}\n")
            return

        self._grating_modes[grating]["a0"] = getval_float(
            "A0 (mm)", default=self._grating_modes[grating]["a0"]
        )
        self._grating_modes[grating]["a1"] = getval_float(
            "A1 (mm.mm)", default=self._grating_modes[grating]["a1"]
        )
        self._grating_modes[grating]["a2"] = getval_float(
            "A2 (mm.mm.mm)", default=self._grating_modes[grating]["a2"]
        )
        self._grating_modes[grating]["radius"] = getval_float(
            "Radius (mm)", default=self._grating_modes[grating]["radius"]
        )
        self._grating_modes[grating]["axis_name"] = getval_name(
            "Grating motor name", default=self._grating_modes[grating]["axis_name"]
        )
        self._grating_modes[grating]["offset"] = getval_float(
            "Offset (deg)", default=self._grating_modes[grating]["offset"]
        )

        self._grating_offsets["grtx"] = getval_float(
            "Offset grtx (mm)", default=self._grating_offsets["grtx"]
        )
        self._grating_offsets["detx"] = getval_float(
            "Offset detx (mm)", default=self._grating_offsets["detx"]
        )
        self._grating_offsets["detz"] = getval_float(
            "Offset detz (mm)", default=self._grating_offsets["detz"]
        )

    def spectro_set(self, energy=None, grating_angle=None, r1=-1):
        """ """
        self._spectro_calc(energy, grating_angle, r1, True)

    def spectro_calc(self, energy=None, grating_angle=None, r1=-1):
        """ """
        self._spectro_calc(energy, grating_angle, r1, False)

    def _spectro_calc(self, energy=None, grating_angle=None, r1=-1, move=False):

        if energy is None:
            print("Usage: spectro_set energy(eV) [r1[mm]]")
            return
        config = get_config()
        grating_mot = config.get(
            self._grating_modes[self.grating_selected]["axis_name"]
        )
        if grating_angle is None:
            grating_angle = grating_mot.position

        mots = self._calc_followers(energy, grating_angle, r1)

        print(
            f"\bAlpha = {mots['alpha']:9.3f}\nBeta  = {mots['beta']:9.3f}\nR1    = {mots['r1']:9.3f}\nR2    = {mots['r2']:9.3f}\n"
        )
        print(
            tabulate.tabulate(
                [
                    [
                        "Current",
                        self._tagged["detx"][0].position,
                        self._tagged["detz"][0].position,
                        self._tagged["grtx"][0].position,
                        self._tagged["r1"][0].position,
                        self._tagged["r2"][0].position,
                    ],
                    [
                        "Move to" if move else "Calculated",
                        mots["detx"],
                        mots["detz"],
                        mots["grtx"],
                        mots["r1"],
                        mots["r2"],
                    ],
                ],
                ("", "detx", "detz", "grtx", "r1", "r2"),
                floatfmt="9.3f",
            )
        )

        if move:
            umv(
                self._tagged["detx"][0],
                mots["detx"],
                self._tagged["detz"][0],
                mots["detz"],
                self._tagged["grtx"][0],
                mots["grtx"],
            )

    def _calc_followers(self, energy, grating_angle, fix_r1):
        pos = {}

        _lambda = self._calc_lambda(energy)
        alpha = self._calc_alpha(grating_angle)
        beta = self._calc_beta(alpha, _lambda)

        bprim = self._calc_bprim(alpha, beta, _lambda)
        cste = self._calc_cste(beta, bprim, _lambda)

        blin = self._calc_blin(alpha, beta, bprim)
        aquad = self._calc_aquad(alpha, beta)
        delta = self._calc_delta(blin, aquad, cste)

        if fix_r1 < 0:
            r1 = self._calc_r1(blin, aquad, delta)
        else:
            r1 = fix_r1 / 1000
        r2 = self._calc_r2(alpha, beta, _lambda, r1)

        deflection = numpy.pi - alpha - beta

        ltot = r1 + r2 * numpy.cos(deflection)
        height = r2 * numpy.sin(deflection)

        pos["alpha"] = alpha
        pos["beta"] = beta
        pos["r1"] = r1 * 1000
        pos["r2"] = r2 * 1000

        pos["grtx"] = (r1 - self._grating_offsets["grtx"]) * 1000
        pos["detx"] = (ltot - self._grating_offsets["detx"]) * 1000
        pos["detz"] = (height - self._grating_offsets["detz"]) * 1000

        return pos

    def _calc_lambda(self, energy):

        _lambda = 12398.4187e-10 / energy
        return _lambda

    def _calc_alpha(self, grating_angle):

        offset = self._grating_modes[self.grating_selected]["offset"]

        alpha = numpy.pi / 2 - (numpy.deg2rad(grating_angle) + numpy.deg2rad(offset))
        return alpha

    def _calc_beta(self, alpha, _lambda):

        a0 = self._grating_modes[self.grating_selected]["a0"] * 1000
        beta = numpy.arcsin(numpy.sin(alpha) - _lambda * a0)
        return beta

    def _calc_bprim(self, alpha, beta, _lambda):

        radius = self._grating_modes[self.grating_selected]["radius"] / 1000
        a1 = self._grating_modes[self.grating_selected]["a1"] * 1000 * 1000
        cos_a = numpy.cos(alpha)
        cos_b = numpy.cos(beta)

        bprim = ((cos_a + cos_b) / radius + _lambda * a1) / cos_b / cos_b
        return bprim

    def _calc_cste(self, beta, bprim, _lambda):

        radius = self._grating_modes[self.grating_selected]["radius"] / 1000
        a2 = self._grating_modes[self.grating_selected]["a2"] * 1000 * 1000 * 1000
        sin_2b = numpy.sin(2 * beta)
        cos_b = numpy.cos(beta)

        cste = (
            sin_2b * cos_b * bprim * bprim
            - sin_2b * bprim / radius
            - 4 * _lambda * a2 / 3
        )
        return cste

    def _calc_blin(self, alpha, beta, bprim):

        radius = self._grating_modes[self.grating_selected]["radius"] / 1000
        cos_a = numpy.cos(alpha)
        cos_b = numpy.cos(beta)
        cos_ab2 = cos_a * cos_a / cos_b / cos_b
        sin_2a = numpy.sin(2 * alpha)
        sin_2b = numpy.sin(2 * beta)

        blin = (
            sin_2a + sin_2b * cos_ab2
        ) / radius - 2 * sin_2b * cos_b * cos_ab2 * bprim
        return blin

    def _calc_aquad(self, alpha, beta):

        cos_a = numpy.cos(alpha)
        cos_b = numpy.cos(beta)
        sin_2a = numpy.sin(2 * alpha)
        sin_2b = numpy.sin(2 * beta)
        cos_ab2 = cos_a * cos_a / cos_b / cos_b

        aquad = -sin_2a * cos_a + sin_2b * cos_b * cos_ab2 * cos_ab2
        return aquad

    def _calc_delta(self, blin, aquad, cste):

        delta = blin * blin - 4 * aquad * cste
        return delta

    def _calc_r1(self, blin, aquad, delta):

        r1 = -2 * aquad / (numpy.sqrt(delta) + blin)

        return r1

    def _calc_r2(self, alpha, beta, _lambda, r1):

        radius = self._grating_modes[self.grating_selected]["radius"] / 1000
        a1 = self._grating_modes[self.grating_selected]["a1"] * 1000 * 1000
        cos_a = numpy.cos(alpha)
        cos_b = numpy.cos(beta)

        r2 = (
            cos_b
            * cos_b
            / ((cos_a + cos_b) / radius - cos_a * cos_a / r1 + _lambda * a1)
        )

        return r2
