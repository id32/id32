# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


"""
Motion hook to manage Brakes, Airpad and wago interlock security 
    to move the RIXS arms, on motors detx, detz(linked detzu and detzd) grtx and

hooks:
  - name: rixs_security
    plugin: generic
    package: id32.controllers.rixs_security
    class: RixsSecurityHook
    wago_brakes: $wcid32k
    wago_airpad: $wcid32l
    brake_timeout: 2.
    air_timeout: 20.

"""

import gevent, time, tabulate
from bliss.common.hook import MotionHook


class RixsSecurityHook(MotionHook):
    """
    Manage Brakes and Airpad and wago interlock security
    to move the RIXS arms
    """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.wago_brakes = config.get("wago_brakes")
        self.wago_airpad = config.get("wago_airpad")
        self.air_timeout = config.get("air_timeout", 2)
        self.brake_timeout = config.get("brake_timeout", 20)
        super().__init__()

    def pre_move(self, motion_list):

        for motion in motion_list:
            if motion.axis.name in ["detx", "detz", "grtx"]:
                self.brake_open(motion.axis.name)
                gevent.sleep(0.5)

            elif motion.axis.name == "rtth":
                self.airpad_on()
                self.airmot_on()
                gevent.sleep(0.5)
                if (
                    self.wago_airpad.get("ilckl_tth_plus") == 0
                    or self.wago_airpad.get("ilckl_tth_minus") == 0
                ):
                    raise ValueError(
                        "Movement not allowed on rtth, check the interlocks..."
                    )

    def post_move(self, motion_list):

        for motion in motion_list:
            if motion.axis.name in ["detx", "detz", "grtx"]:
                gevent.sleep(0.5)
                self.brake_close(motion.axis.name)

            elif motion.axis.name == "rtth":
                self.airpad_off()
                self.airmot_off()

    def brake_release(self, axis_name, value=True):
        if axis_name not in ["detx", "detz", "grtx"]:
            raise ValueError("brake_release(): Allowed motors: detx - detz - grtx")

        value = 1 if value == True else 0
        self.wago_brakes.set(f"cmd_{axis_name}", value)
        gevent.sleep(0.1)

        br_state = 0 if value == 1 else 1
        cur_state = self.wago_brakes.get(f"ilckk_{axis_name}")
        cur_status = "OPEN" if cur_state == 1 else "CLOSED"
        print(f"{axis_name} brakes are {cur_status}\r")
        start = time.time()
        while cur_state == br_state:
            gevent.sleep(0.1)
            cur_state = self.wago_brakes.get(f"ilckk_{axis_name}")
            cur_status = "OPEN" if cur_state == 1 else "CLOSED"
            print(f"{axis_name} brakes are {cur_status}\r")
            if time.time() - start > self.brake_timeout:
                br_status = "OPEN" if value == 1 else "CLOSED"
                raise TimeoutError(
                    f"Timeout ({self.brake_timeout} sec.) before setting {axis_name} brakes to {br_status}"
                )

    def brake_close(self, axis_name):
        """
        Switch the motor brakes closed
        """
        self.brake_release(axis_name, False)

    def brake_open(self, axis_name):
        """
        Switch the motor brakes open
        """
        self.brake_release(axis_name, True)

    def airpad_on(self):
        """
        Switch the motor airpad ON
        """

        self.wago_airpad.set("airpad", 1)
        cur_state = self.wago_airpad.get("st_airpad")
        cur_status = "ON" if cur_state == 1 else "OFF"
        print(f"AIRPAD is {cur_status}\r")
        start = time.time()
        while cur_state == 0:
            cur_state = self.wago_airpad.get("st_airpad")
            cur_status = "ON" if cur_state == 1 else "OFF"
            print(f"AIRPAD is {cur_status}\r")
            if time.time() - start > self.air_timeout:
                self.wago_airpad.set("airpad", 0)
                raise TimeoutError(
                    f"Timeout ({self.air_timeout} sec.) before setting AIRPAD to ON, AIRPAD forced to OFF!!"
                )

    def airpad_off(self):
        """
        Switch the motor airpad  OFF
        """

        self.wago_airpad.set("airpad", 0)
        cur_state = self.wago_airpad.get("st_airpad")
        cur_status = "ON" if cur_state == 1 else "OFF"
        print(f"AIRPAD is {cur_status}\r")
        start = time.time()
        while cur_state == 1:
            cur_state = self.wago_airpad.get("st_airpad")
            cur_status = "ON" if cur_state == 1 else "OFF"
            print(f"AIRPAD is {cur_status}\r")
            if time.time() - start > self.air_timeout:
                self.wago_airpad.set("airpad", 0)
                raise TimeoutError(
                    f"Timeout ({self.air_timeout} sec.) before setting AIRPAD to OFF, AIRPAD forced to ON!!"
                )

    def airmot_on(self):
        """
        Switch the motor (rtth) air ON
        """

        self.wago_airpad.set("airmot", 1)
        gevent.sleep(0.5)
        cur_state = self.wago_airpad.get("st_airmot")
        cur_status = "ON" if cur_state == 1 else "OFF"
        print(f"AIRMOT is {cur_status}\r")
        start = time.time()
        while cur_state == 0:
            cur_state = self.wago_airpad.get("st_airmot")
            cur_status = "ON" if cur_state == 1 else "OFF"
            print(f"AIRMOT is {cur_status}\r")
            if time.time() - start > self.air_timeout:
                self.wago_airpad.set("airmot", 0)
                raise TimeoutError(
                    f"Timeout ({self.air_timeout} sec.) before setting AIRMOT to OFF, AIRMOT forced to ON!!"
                )

    def airmot_off(self):
        """
        Switch the motor (rtth) air OFF
        """

        self.wago_airpad.set("airmot", 0)
        gevent.sleep(0.5)
        cur_state = self.wago_airpad.get("st_airmot")
        cur_status = "ON" if cur_state == 1 else "OFF"
        print(f"AIRMOT is {cur_status}\r")
        start = time.time()
        while cur_state == 0:
            cur_state = self.wago_airpad.get("st_airmot")
            cur_status = "ON" if cur_state == 1 else "OFF"
            print(f"AIRMOT is {cur_status}\r")
            if time.time() - start > self.air_timeout:
                self.wago_airpad.set("airmot", 0)
                raise TimeoutError(
                    f"Timeout ({self.air_timeout} sec.) before setting AIRMOT to OFF, AIRMOT forced to ON!!"
                )

    def __info__(self):

        info = "============= RIXS security STATUS ==============\n\n"
        motors = ["detx", "grtx", "detzu", "detzd"]
        interlocks = ["Brake Interlock"]
        status = ["Brake Status"]
        commands = ["Brake Command"]
        moves = ["Move"]
        icepaps = ["icepap"]
        for mot in motors:
            interlocks.append(
                "OPEN" if self.wago_brakes.get(f"brk_{mot}") == 1 else "CLOSED"
            )
            status.append(
                "OPEN" if self.wago_brakes.get(f"st_brk_{mot}") == 1 else "CLOSED"
            )
            commands.append(
                "OPEN" if self.wago_brakes.get(f"cmd_{mot}") == 1 else "CLOSED"
            )
            moves.append("OK" if self.wago_brakes.get(f"ilckk_{mot}") == 1 else "NOTOK")
            icepaps.append("ON" if self.wago_brakes.get(f"iceon_{mot}") == 1 else "OFF")
        motors.insert(0, "")
        info += tabulate.tabulate(
            [interlocks, status, commands, moves, icepaps], tuple(motors)
        )

        airs = ["Airpad", "Airmot"]
        commands = ["Command"]
        status = ["Status"]
        for air in airs:
            commands.append(
                "ON" if self.wago_airpad.get(f"{air.lower()}") == 1 else "OFF"
            )
            status.append(
                "ON" if self.wago_airpad.get(f"st_{air.lower()}") == 1 else "OFF"
            )
        airs.insert(0, "")
        info += "\n\n" + tabulate.tabulate([commands, status], tuple(airs))

        motors = ["tth", "army", "lm"]
        limit_plus = ["Limit Plus"]
        limit_minus = ["Limit Minus"]
        for motor in motors:
            limit_plus.append(
                "ON" if self.wago_airpad.get(f"ilckl_{motor}_plus") == 1 else "OFF"
            )
            limit_minus.append(
                "ON" if self.wago_airpad.get(f"ilckl_{motor}_minus") == 1 else "OFF"
            )

        header = (
            "",
            "TTH",
            "ARMY",
            "FORK",
        )
        info += "\n\n" + tabulate.tabulate([limit_plus, limit_minus], header)

        return info
