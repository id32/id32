from tango import DevState, GreenMode
from tango.server import Device, device_property, attribute, command
from bliss.config import static

# from bliss.controllers.preciamolen_i5 import I5
from id32.controllers.preciamolen_i5 import I5


"""
property
beacon_name --> i5
"""


class PreciamolenI5(Device):
    beacon_name = device_property(dtype="str")

    def __init__(self, *args, **kwargs):
        self._i5 = None
        super().__init__(*args, **kwargs)

    def init_device(self):
        """Initialise the tango device"""
        super().init_device()
        self.set_state(DevState.FAULT)
        self.get_device_properties(self.get_device_class())
        config = static.get_config()
        self.__i5 = config.get_config(self.beacon_name)
        # remove tango url
        self.__i5.pop("tango")
        self._i5 = I5(self.beacon_name, self.__i5)
        self.set_state(DevState.ON)

    @attribute(dtype=("float",), max_dim_x=3, label="Measure Raw/Tare/Net")
    def measure(self):
        try:
            measure = self._i5.weights()
            values = list()
            for key in sorted(measure):
                values.append(measure[key])
        except Exception as e:
            raise e

        return values

    @attribute(dtype=("str"))
    def unit(self):
        return self._i5.unit

    @command(dtype_in=[str], dtype_out=[str])
    def io(self, cmd):
        if len(cmd) == 1 and cmd[0] == "":
            cmd = None
        return self._i5._io(cmd)


def main(args=None, **kwargs):
    from tango.server import run

    kwargs["green_mode"] = kwargs.get("green_mode", GreenMode.Gevent)
    return run((PreciamolenI5,), args=args, **kwargs)


if __name__ == "__main__":
    main()
