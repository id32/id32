# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import gevent, numpy, tabulate
from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.config.static import get_config

"""
RIXS  fourc diffractometer omega calc axis: omega = th - tth/2
Omega is a mandatory axis for the fourc geometry 

WARNING: tth axis is not passed as a real motor in this calc otherwise bliss will complain
with motor group where hkl reference both tth and omega as real motors and where omega depends
on tth real motor as well.

yml configuration example:

- class: OmegaController          # object class (inheriting from CalcController)
  plugin: generic                      # CalcController works with generic plugin
  package: id32.controllers.omega_cmot # module where the FooCalcController class can be found
  name: omega_ctrl                    # a name for the controller (optional)
  tth: $tth 

  axes:
    - name: $th
      tags: real th

    - name: omega            # a name for a virtual axis (output)
      tags: omega            # a tag for identification within the controller
"""


class OmegaController(CalcController):
    def __init__(self, config):
        CalcController.__init__(self, config)
        self.tth = config.get("tth")

    def calc_from_real(self, real_user_positions):
        """Computes pseudo dial positions from real user positions.

        => pseudo_dial_positions = self.calc_from_real(real_user_positions)

        Args:
            real_user_positions: { real_tag: real_user_positions, ... }
        Return:
            a dict: {pseudo_tag: pseudo_dial_positions, ...}

        """
        _tth_pos = self.tth.position
        _omega = real_user_positions["th"] - _tth_pos / 2
        return {"omega": _omega}

    def calc_to_real(self, pseudo_dial_positions):
        """Computes reals user positions from pseudo dial positions.

        => real_user_positions = self.calc_to_real(pseudo_dial_positions)

        Args:
            pseudo_dial_positions: {pseudo_tag: pseudo_dial_positions, ...}
        Return:
            a dict: { real_tag: real_user_positions, ... }
        """

        _omega_pos = pseudo_dial_positions["omega"]
        _last_tth_pos = self.tth.position
        _th_pos = _omega_pos + _last_tth_pos / 2

        return {"th": _th_pos}
