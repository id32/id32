# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import numpy as np

from tango import DevState, GreenMode, DebugIt
from tango import DevVarStringArray, DevString
from tango.server import Device, attribute, command, run
from tango.server import device_property

from id32.controllers.cryogenic_magnet_ps import CryogenicPSDevice

from bliss.config import static


def switch_state(tg_dev, state=None, status=None):
    """Helper to switch state and/or status and send event"""
    if state is not None:
        tg_dev.set_state(state)
        if state in (DevState.ALARM, DevState.UNKNOWN, DevState.FAULT):
            msg = "State changed to " + str(state)
            if status is not None:
                msg += ": " + status
    if status is not None:
        tg_dev.set_status(status)


class CryogenicPSServer(Device):

    beacon_name = device_property(dtype="str")

    @DebugIt()
    def init_device(self, *args, **kwargs):
        super().init_device(*args, **kwargs)
        switch_state(self, DevState.STANDBY)
        self.device = None
        if self.beacon_name:
            config = static.get_config()
            self.yml_config = config.get_config(self.beacon_name)
            if self.yml_config is None:
                msg = f"Could not find a Beacon object with name {self.beacon_name}"
                switch_state(self, DevState.FAULT, msg)
                return
            try:
                # remove tango url, otherwise get_comm will complain about 2 channels found
                self.yml_config.pop("tango")
                self.device = CryogenicPSDevice(self.yml_config)
            except Exception as e:
                msg = f"Exception initializing device: {e}"
                switch_state(self, DevState.FAULT, msg)
                return

    @DebugIt()
    def delete_device(self):
        if self.device:
            pass

    #
    # device properties to attributes mapping
    #
    @attribute(dtype=float, doc="Current magnet field in Tesla")
    @DebugIt()
    def magnet_field(self):
        return self.device.magnet_field

    @attribute(dtype=float, doc="Target magnet field in Tesla")
    @DebugIt()
    def target_field(self):
        return self.device.target_field

    @target_field.setter
    @DebugIt()
    def target_field(self, value):
        self.device.target_field = value

    @attribute(dtype=float, doc="Maximum field in Tesla")
    @DebugIt()
    def max_field(self):
        return self.device.max_field

    @attribute(dtype=float, doc="Voltage limit in Volt")
    @DebugIt()
    def voltage_limit(self):
        return self.device.voltage_limit

    @attribute(dtype=float, doc="Field constant in Tesla per Ampere")
    @DebugIt()
    def field_constant(self):
        return self.device.field_constant

    @attribute(dtype=bool, doc="Return if pause is on(True) or off(False)")
    def pause(self):
        return self.device.pause

    @pause.setter
    @DebugIt()
    def pause(self, value):
        self.device.pause = value

    @attribute(
        dtype=bool,
        doc="Return if current direction is positive(True) or negative(False)",
    )
    def direction_positive(self):
        return self.device.direction_positive

    @direction_positive.setter
    @DebugIt()
    def direction_positive(self, value):
        self.device.direction_positive = value

    @attribute(dtype=float, doc="Target ramp rate in T/min")
    def ramp_rate(self):
        return self.device.ramp_rate

    @ramp_rate.setter
    @DebugIt()
    def ramp_rate(self, value):
        self.device.ramp_rate = value

    @attribute(dtype=int, doc="Current channel number: 0-9T, 1-4T")
    def channel(self):
        return self.device.channel

    @channel.setter
    @DebugIt()
    def channel(self, value):
        self.device.channel = value

    @attribute(dtype=str, doc="Ramping status, Holding or Ramping")
    def ramping_status(self):
        return self.device.ramping_status

    @attribute(
        dtype=[
            str,
        ],
        max_dim_x=20,
        doc="Long status info.",
    )
    def update(self):
        return self.device.update

    #
    # Device methods to commands mapping
    #

    @command(
        dtype_in=int,
        doc_in="channel",
        dtype_out=float,
        doc_out="the last set ramp_rate",
    )
    @DebugIt()
    def get_channel_ramp_rate(self, channel):
        return self.device.get_channel_ramp_rate(channel)

    @command()
    @DebugIt()
    def ramp_to_zero(self):
        self.device.ramp_to_zero()

    @command()
    @DebugIt()
    def ramp_to_target(self):
        self.device.ramp_to_target()

    @command()
    @DebugIt()
    def ramp_to_max(self):
        self.device.ramp_to_max()

    @command(
        dtype_in=DevString,
        doc_in="command",
        dtype_out=DevVarStringArray,
        doc_out="array of command answers",
    )
    @DebugIt()
    def send_cmd(self, cmd):
        asw = []
        self.device.comm.flush()
        ret = self.device.send_cmd(cmd)
        if ret:
            asw += ret
        return asw


def main():
    run([CryogenicPSServer], green_mode=GreenMode.Gevent)


if __name__ == "__main__":
    main()
