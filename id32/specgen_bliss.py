#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import socket
import json


def extract_spectra(directory, prefix, suffix, output_dir, first_image, 
    last_image, slope, points_per_pixel, binning, lower_threshold, 
    upper_threshold, masksize, SPC, SPC_gridsize, SPC_low_threshold,
    SPC_high_threshold, SPC_single_threshold, SPC_double_threshold, label,
    scan_number):
    
    import os
    from RixsSpectrum import RixsSpectrum
    import time    

    if last_image == -1:
        last_image = len(os.listdir(directory))
    
    fnames = ['%s/%s' % (directory, f) for f in sorted(os.listdir(directory)) 
        if f in ['%s%04d%s' % (prefix, iii, suffix) 
            for iii in range(first_image, last_image+1)]]
    
    for i, fname in enumerate(fnames):
        x = RixsSpectrum(fname, slope=slope, smile=-1.3e-6, lower_threshold=lower_threshold, 
        upper_threshold=upper_threshold, points_per_pixel=points_per_pixel, 
        masksize=masksize, SPC=SPC, SPC_gridsize=SPC_gridsize, 
        SPC_low_threshold=SPC_low_threshold, 
        SPC_high_threshold=SPC_high_threshold, 
        SPC_single_threshold=SPC_single_threshold,
        SPC_double_threshold=SPC_double_threshold)
        
        outputfile = '%s/%s%s.spec' % (output_dir, prefix, label)
        x.save(outputfile=outputfile, savedatfile=False)
        outputfile = '%s/Scan_%04d_%s%s.spec' % (output_dir, scan_number, prefix, label)
        x.save(outputfile=outputfile, savedatfile=False)

    
    return 0


def send_specgen_request(path, point_number, scan_info):
    
    label = ''
    
    if label != 'pol':
        PORT = 50098
    else:
        PORT = 50099
    
    HOSTS = ['', 'pckummer.esrf.fr']
    HOSTS = ['']
    
    for HOST in HOSTS:
        
        # ~ try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((HOST, PORT))
        
        data_send = [path, point_number, scan_info]
        s.sendall(json.dumps(data_send))
        print("\033[1mImage processing request sent to SpecGen server...\033[0m", end='')
        data_recv = s.recv(1024)
        s.close()
        if json.loads(data_recv) == data_send:
            print('\033[1m OK.\033[0m')
        else:
            print('\nRequest failed. Server received', repr(data))
        # ~ except OSError as exc:
            # ~ #~ print('WARNING: Caught exception socket.error : %s' % exc)
            # ~ print('WARNING: Cannot connect to SpecGen server. Calling SpecGen directly.')
            # ~ print('\033[1mHINT: Start the SpecGen server to speed up data acquisition ...\033[0m')
            
            
        
    return 0

if __name__ == '__main__':
    main()

