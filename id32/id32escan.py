import gevent
import numpy
import functools
import enum

from bliss import global_map
from bliss.config import settings
from bliss.shell.standard import umv
from bliss.config.static import get_config
from bliss.common.cleanup import cleanup
from bliss.common.logtools import log_debug, log_info
from bliss.controllers.monochromator.utils import UserList
from bliss.controllers.monochromator.monochromator import MonochromatorBase

from fscan.mussttools import MusstChanUserCalc
from fscan.fscantools import FScanTrigMode, FScanMode

from energy_scan.escan import EnergyScans


class ID32EnergyScans(EnergyScans):
    def __init__(self, config):
        super().__init__(config)
        self._ps35b = get_config().get("PS35B")
        self._energy_current_pos = None

    def _cleanup_scan(self):
        if self._energy_current_pos is not None:
            umv(self._mono._motors["energy_tracker"], self._energy_current_pos)
        self._ps35b.tracking_default()

    def energy(
        self,
        Estart,
        Estop,
        npoints,
        time_per_point,
        *counters,
        nscans=1,
        backnforth=False,
        show_time=False,
        comment=None,
        save=True,
        acq_during_acc_dec=False,
        acc_margin=0,
    ):

        if self.cst_speed_mode._value == "UNDULATOR":
            undulator = self.undulator_master._value
            if not undulator.tracking.state:
                print(RED(f"Undulator Master {undulator.name} tracking mode is NOT ON"))
                return

        # ID32 specific
        self._energy_current_pos = self._mono._motors["energy_tracker"].position
        self._ps35b.tracking_off()

        with cleanup(self._cleanup_scan):

            self._traj_setup(Estart, Estop, npoints, time_per_point)

            # TrigScan parameters
            self._trig_scan.trigscan.pars.motor = self._mono
            self._trig_scan.trigscan.pars.start_pos = Estart
            self._trig_scan.trigscan.pars.stop_pos = Estop
            self._trig_scan.trigscan.pars.npoints = npoints
            self._trig_scan.trigscan.pars.acq_time = time_per_point
            self._trig_scan.trigscan.pars.expo_time = self.lima_expo_time
            self._trig_scan.trigscan.pars.sampling_time = self.sampling_time
            self._trig_scan.trigscan.pars.save_flag = save
            self._trig_scan.trigscan.pars.nscans = nscans
            self._trig_scan.trigscan.pars.backnforth = backnforth
            self._trig_scan.trigscan.pars.show_time = show_time
            self._trig_scan.trigscan.pars.sync_motors = True

            if acq_during_acc_dec:
                self._trig_scan.trigscan.pars.start_mode = FScanMode.POSITION
                self._trig_scan.trigscan.pars.scan_mode = FScanMode.POSITION
            else:
                self._trig_scan.trigscan.pars.start_mode = FScanMode.POSITION
                self._trig_scan.trigscan.pars.scan_mode = FScanMode.TIME

            self._trig_scan.trigscan.pars.velocity = None
            self._trig_scan.trigscan.pars.acc_margin = acc_margin
            if acq_during_acc_dec:
                self._trig_scan.trigscan.pars.undershoot = acc_margin
            else:
                self._trig_scan.trigscan.pars.undershoot = None

            if len(counters) > 0:
                self._trig_scan.trigscan.pars.counters = counters
            else:
                self._trig_scan.trigscan.pars.counters = None

            self._scan_info = self._get_scan_info(comment)
            self._run()


class EnergyScansEsrfDCM(EnergyScans):
    def __init__(self, config):
        super().__init__(config)
        self._scan_type = "energy"

    def _cleanup_scan(self):
        if self._scan_type == "energy":
            mcoil = self._mono._motors["mcoil"]
            mtraj = self._mono._motors["trajectory"]
            if mcoil in mtraj.disabled_axes:
                mtraj.enable_axis(mcoil)
        self._mono.thesync(silent=True)
        self._scan_type = "energy"

    def fjsry(self, delta, npoints, time_per_point, *counters, comment=None, save=True):

        with cleanup(self._cleanup_scan):

            fjsry_pos = self._mono._motors["fjsry"].position
            fjsry_start = fjsry_pos - delta
            fjsry_stop = fjsry_pos + delta

            self._mono._load_fjsry_trajectory(
                fjsry_start, fjsry_stop, npoints, time_per_point
            )

            # set virtual energy motor to start position to avoid a long and useless movement
            self._mono._motors["virtual_energy"].position = fjsry_start
            self._mono._motors["virtual_energy"].offset = 0

            # Move trajectory motor on the trajectory
            self._mono._motors["fjsry"].move(fjsry_start)
            self._mono._motors["trajectory"].move(fjsry_start)

            self._trig_scan.trigscan.pars.motor = self._mono
            self._trig_scan.trigscan.pars.start_pos = fjsry_start
            self._trig_scan.trigscan.pars.stop_pos = fjsry_stop
            self._trig_scan.trigscan.pars.npoints = npoints
            self._trig_scan.trigscan.pars.acq_time = time_per_point
            self._trig_scan.trigscan.pars.expo_time = self.lima_expo_time
            self._trig_scan.trigscan.pars.sampling_time = self.sampling_time
            self._trig_scan.trigscan.pars.save_flag = save
            self._trig_scan.trigscan.pars.nscans = 1
            self._trig_scan.trigscan.pars.scan_mode = FScanMode.TIME
            if len(counters) > 0:
                self._trig_scan.trigscan.pars.counters = counters
            else:
                self._trig_scan.trigscan.pars.counters = None

            if self._mono._goat_ctl.regul.hac.wanted:
                self._mono.regul.on()
            else:
                self._mono.regul.off()

            self._scan_info = self._get_scan_info(comment)

            self._scan_type = "fjsry"

            self._run()

    def fjsz(
        self,
        Fstart,
        Fstop,
        npoints,
        time_per_point,
        *counters,
        nbp_per_mm=1000,
        offset=0.1,
        comment=None,
        save=True,
    ):

        with cleanup(self._cleanup_scan):
            # Build Trajectory
            if Fstart < Fstop:
                traj_start = Fstart - offset
                traj_stop = Fstop + offset
            else:
                traj_start = Fstop - offset
                traj_stop = Fstart + offset

            self._mono._load_fjsz_trajectory(traj_start, traj_stop, nbp_per_mm)

            # set virtual energy motor to start position to avoid a long and useless movement
            self._mono._motors["virtual_energy"].position = Fstart
            self._mono._motors["virtual_energy"].offset = 0

            # Move trajectory motor on the trajectory
            umv(self._mono._motors["fjsz"], Fstart)
            umv(self._mono._motors["trajectory"], Fstart)

            self._trig_scan.trigscan.pars.motor = self._mono
            self._trig_scan.trigscan.pars.start_pos = Fstart
            self._trig_scan.trigscan.pars.stop_pos = Fstop
            self._trig_scan.trigscan.pars.npoints = npoints
            self._trig_scan.trigscan.pars.acq_time = time_per_point
            self._trig_scan.trigscan.pars.expo_time = self.lima_expo_time
            self._trig_scan.trigscan.pars.sampling_time = self.sampling_time
            self._trig_scan.trigscan.pars.save_flag = save
            self._trig_scan.trigscan.pars.nscans = 1
            self._trig_scan.trigscan.pars.scan_mode = FScanMode.TIME
            if len(counters) > 0:
                self._trig_scan.trigscan.pars.counters = counters
            else:
                self._trig_scan.trigscan.pars.counters = None

            if self._mono._goat_ctl.regul.hac.wanted:
                self._mono.regul.on()
            else:
                self._mono.regul.off()

            self._scan_info = self._get_scan_info(comment)

            self._scan_type = "fjsrz"

            self._run()


class MusstDiffRawCalcChannel(MusstChanUserCalc):
    def __init__(self, name, config):
        self._name = name
        self._a_name = config.get("a") + "_raw"
        self._b_name = config.get("b") + "_raw"

    def select_input_channels(self, channels_list, scan_pars):
        return [self._a_name, self._b_name]

    def get_output_channels(self):
        return [self._name]

    def calculate(self, input_data_dict):
        a = input_data_dict[self._a_name]
        b = input_data_dict[self._b_name]
        ab_data = {self._name: a - b}
        return ab_data


class MusstDiffCalcChannel(MusstChanUserCalc):
    def __init__(self, name, config):
        self._name = name
        self._a_name = config.get("a") + "_trig"
        self._b_name = config.get("b") + "_trig"

    def select_input_channels(self, channels_list, scan_pars):
        return [self._a_name, self._b_name]

    def get_output_channels(self):
        return [self._name]

    def calculate(self, input_data_dict):
        a = input_data_dict[self._a_name]
        b = input_data_dict[self._b_name]
        ab_data = {self._name: a - b}
        return ab_data


class MusstIntTimeCalcChannel(MusstChanUserCalc):
    def __init__(self, name, config):
        self._name = name
        self.input_name = "timer_trig"

    def prepare(self):
        self._aux_data = numpy.array((), dtype=numpy.float64)
        self._first_time = True
        self._nbp_sent = 0
        self._nscan_sent = 0

    def select_input_channels(self, channels_list, scan_pars):
        return [self.input_name]

    def get_output_channels(self):
        return ["timer_int"]

    def calculate(self, input_data_dict):
        data = input_data_dict[self.input_name]
        self._aux_data = numpy.append(self._aux_data, data)
        if len(self._aux_data) < 2:
            return {"timer_int": []}
        inttime_data = self._aux_data[1:] - self._aux_data[:-1]
        if self._first_time:
            first_point = inttime_data[0]
            inttime_data = numpy.insert(inttime_data, 0, first_point)
            if (self._nbp_sent + len(inttime_data)) % (
                self._scan_pars.npoints + 1
            ) == 0:
                self._aux_data = numpy.array((), dtype=numpy.float64)
                self._first_time = True
            else:
                self._aux_data = self._aux_data[-1:]
                self._first_time = False
        else:
            nbp_sent = self._nbp_sent + len(inttime_data)
            nscan_sent = int(nbp_sent / (self._scan_pars.npoints + 1))
            if nscan_sent > self._nscan_sent:
                ind = nscan_sent * (self._scan_pars.npoints + 1) - self._nbp_sent
                if ind >= len(inttime_data):
                    self._first_time = True
                    self._aux_data = numpy.array((), dtype=numpy.float64)
                else:
                    if ind == len(inttime_data) - 1:
                        inttime_data = inttime_data[:-1]
                        self._first_time = True
                    else:
                        inttime_data[ind] = inttime_data[ind + 1]
                    self._aux_data = self._aux_data[-1:]
            else:
                self._aux_data = self._aux_data[-1:]
        self._nbp_sent += len(inttime_data)
        self._nscan_sent = int(self._nbp_sent / (self._scan_pars.npoints + 1))
        return {"timer_int": inttime_data}


class MusstEnergyCalcChannel(MusstChanUserCalc):
    def __init__(self, name, config):
        self._name = name
        self.mono = config.get("monochromator", None)
        if self.mono is None:
            raise RuntimeError(
                f"MusstEnergyCalcChannel ({name}): No Monochromator specified"
            )
        self.input_name = self.mono._motors["bragg_real"].name + "_trig"

    def select_input_channels(self, channels_list, scan_pars):
        return [self.input_name]

    def get_output_channels(self):
        return ["energy_enc"]

    def calculate(self, input_data_dict):
        bragg_angle = input_data_dict[self.input_name] + self.mono.bragg_offset
        ene_data = {"energy_enc": self.mono.bragg2energy(bragg_angle)}
        return ene_data


class MusstEnergyCenterCalcChannel(MusstChanUserCalc):
    def __init__(self, name, config):
        self._name = name
        self.mono = config.get("monochromator", None)
        if self.mono is None:
            raise RuntimeError(
                f"MusstEnergyCalcChannel ({name}): No Monochromator specified"
            )
        self.input_name = self.mono._motors["bragg_real"].name + "_center"

    def select_input_channels(self, channels_list, scan_pars):
        return [self.input_name]

    def get_output_channels(self):
        return ["energy_enc"]

    def calculate(self, input_data_dict):
        bragg_angle = input_data_dict[self.input_name] + self.mono.bragg_offset
        ene_data = {"energy_enc": self.mono.bragg2energy(bragg_angle)}
        return ene_data
