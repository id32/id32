# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# Single source of truth for the version number and the like


# Original code comes from ID31, id31.git/id31/id31musst.py
# L.Claustre 3/09/2020


from bliss.controllers.musst import musst, MusstIntegratingCounterController
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

# from bliss.common.auto_filter.acquisition_objects import _Lima
from bliss.common.auto_filter.chain_patching_inplace import (
    AutoFilterLimaAcquisitionMasterPatch,
)
from bliss.scanning.chain import ChainNode


class StepScanAccMusstAcquisitionMaster(MusstAcquisitionMaster):
    def __init__(self, musst_device, program):
        super().__init__(musst_device, program=program)

    def prepare(self):
        # assert len(self.slaves) == 1
        for slave in self.slaves:
            assert isinstance(slave, LimaAcquisitionMaster) or isinstance(
                slave, AutoFilterLimaAcquisitionMasterPatch
            )
            # assert slave.device.device.acquisition.mode == 'ACCUMULATION'

        self.wait_slaves_prepare()

        max_latency = 0
        nframes = None
        max_acc_expo_time = 0
        timer_factor = self.musst.get_timer_factor()
        for lima_slave in self.slaves:
            if isinstance(lima_slave, AutoFilterLimaAcquisitionMasterPatch):
                device = lima_slave.device.device
            else:
                device = lima_slave.device

            if nframes is None:
                nframes = int(device.accumulation.nb_frames)
            else:
                assert nframes == device.accumulation.nb_frames

            max_latency = max(max_latency, device.proxy.latency_time)

            max_acc_expo_time = max(max_acc_expo_time, device.accumulation.expo_time)
        # just a test with dhyana readout time is 42 ms
        max_latency = 0.042
        deadtime = int(max_latency * timer_factor)
        exp_time = int(max_acc_expo_time * timer_factor)

        self.next_vars = {"NFRAMES": nframes, "DEADTIME": deadtime, "EXPTIME": exp_time}

        return MusstAcquisitionMaster.prepare(self)


class SelectiveAcqObjIntegratingCounterController(MusstIntegratingCounterController):
    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        program = acq_params.get("program")
        if program == "acc_step_scan.mprg":
            return StepScanAccMusstAcquisitionMaster(self.musst, program)
        return super().get_acquisition_object(
            acq_params, ctrl_params, parent_acq_params
        )

    def get_default_chain_parameters(self, scan_params, acq_params):
        return {"program": acq_params.get("program")}


class accumulation_musst(musst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.integrating_counters = SelectiveAcqObjIntegratingCounterController(self)
